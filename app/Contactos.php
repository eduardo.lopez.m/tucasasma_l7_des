<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactos extends Model
{
    protected $table = 'contactos';

    protected $fillable = ['name', 'tel', 'email'];
    protected $primaryKey = 'id';
    // protected $hidden = ['updated_at', 'created_at'];

    public function scopeName($query, $name = '') {
        if (!empty($name) and !is_null($name)) {
    	    return $query->where('name', 'LIKE', '%'.$name.'%');
        }
    }

    public function scopeTel($query, $tel = '') {
        if (!empty($tel) and !is_null($tel)) {
    	    return $query->where('tel', 'LIKE', '%'.$tel.'%');
        }
    }

    public function scopeEmail($query, $email = '') {
        if (!empty($email) and !is_null($email)) {
    	    return $query->where('email', 'LIKE', '%'.$email.'%');
        }
    }

    public function scopeDtFechaInicio($query, $created_at = '') {
        if (!empty($created_at and !is_null($created_at))) {
            return $query->where('contactos.created_at', '>=', $created_at.' 00:00:00');
        }
    }

    public function scopeDtFechaFin($query, $created_at = '') {
        if (!empty($created_at and !is_null($created_at))) {
            return $query->where('contactos.created_at', '<=', $created_at.' 23:59:59');
        }
    }
}
