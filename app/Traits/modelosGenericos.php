<?php
namespace App\Traits;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Image;
use Validator; 
trait modelosGenericos{

    public function lista($request){
        if($request->get('ln_nombre')) {
        $ln_nombre = $request->get('ln_nombre');
        $idCajaLista = $request->get('idCajaLista');
        $idregistro = $request->get('idregistro');
        
            $data = self::where('ln_nombre', 'LIKE', "%{$ln_nombre}%") ////DB::table('satCodigoProducto')
            ->where('sn_activo', 1)
            ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            if($data->isNotEmpty()){
            
            foreach($data as $row) {
                $texto = $row->sn_sat.' - '.$row->ln_nombre;
                $output .= '<li class="list-group-item-success"><a onclick="fnAddIdListaGeneral(\''.$row->id.'\', \''.$texto.'\', \''.$idregistro.'\', \''.$idCajaLista.'\')">'.$texto.'</a></li>';
            }
        
            $output .= '</ul>';
            echo $output;
        
            }else{
            
            $output .= '<li class="list-group-item-warning"><a onclick="fnAddIdListaGeneral(\''.'-1'.'\', \''.'No existe en el catálogo'.'\', \''.$idregistro.'\', \''.$idCajaLista.'\')">'.'No existe en el catálogo'.'</a></li>';
            $output .= '</ul>';
            echo $output;
            }
        }
    }
    /*public  function indexPaginacion($nNumPag){
     $data =self::where('sn_activo', '=', 1)->orderBy('updated_at', 'desc')->paginate(3);
     return $data;
    }

    public function ajaxPagina(Request $request){
     if($request->ajax()){
      $data = self::orderBy('updated_at', 'desc')->paginate(3);
      return $data;
     }
    }*/
    /*public function exportarTodo(){
            return self::all();
    }*/ /*
    public function descarga($datos,$nombreExtencion){

        return Excel::download($datos, $nombreExtencion);
    } */
    public function imageSubir($request,$userId){
        //count($request->image) == 1
        $data= ""; 
        
        if (!is_array( $request->file('file'))){
                    $data .=  $this->imagen($request->file('file'),$userId);
                    //$this-> imagenRuta($request->id,$data);
        }else{
            foreach ($request->file('file')  as $file) {//as $key => $value) {
                    $data.=$this->imagen($file,$userId).",";
                   // $this-> imagenRuta($request->id,$data);
            }
                    
        }      
        return $data;
        
    }
    private function imagen($file,$userId){
      
            /*$imageName = time(). $key . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images'), $imageName); */
            $permitidas=['jpg','jpe','jpeg','png','gif','JPG','JPE','JPEG','PNG','GIF' ];
            $extension = $file->getClientOriginalExtension();
            $check=in_array($extension,$permitidas);
            if($check){
                $nombre= $file->getClientOriginalName();
                $nombre = pathinfo($nombre,PATHINFO_FILENAME);
                $nombre = str_replace(' ', '', $nombre);
                $fileName =$userId.$nombre."_".time() . '.' . $extension;
                $path = 'images/temp/'.$fileName;
                //$file->move($path, $fileName);
                Image::make($file)->resize(320, 198)->save($path);
                return 'images/temp/'.$fileName;
            }else{
                //echo "mal";
            }
            

    }
    public function imagenRuta($id,$ruta,$lguardar){
        $data = self::where('id', '=',$id)->get();
        if($lguardar){
            if($data->isNotEmpty()){
                $valor =  $data[0]->stringPath;
                $data[0]->stringPath=$valor.$ruta;
                $data[0]->save(); 
            }
        }else{
              return $data[0]->stringPath;
        }
        
    }

 }
?>