<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoOperaciones extends Model
{
    protected $fillable = ['ln_tipo_operacion','nu_activo'];
    protected $primaryKey = 'nu_tipo_operacion';
    protected $hidden = ['updated_at', 'created_at'];
}
