<?php

namespace App\Http\Controllers;

use App\Contactos;
use App\ContactosMensajes;
use App\Visitas;

use App\Mail\MessageReceived;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ContactoController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $xml = new \DomDocument('1.0', 'UTF-8'); //Se crea el docuemnto

            $raiz = $xml->createElement('Contacto');
            $raiz = $xml->appendChild($raiz);

            // Telefonos
            $nodo_First = $xml->createElement('Titulos');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Principal', $dataRequest['txtPrinicipal']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Descripcion', $dataRequest['txtDescripcion']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            // Links
            $nodo_First = $xml->createElement('Links');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Maps', $dataRequest['txtLinkMaps']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            // Email
            $nodo_First = $xml->createElement('Email');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Llegada', $dataRequest['txtEmailContacto']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            //Se eliminan espacios en blanco
            $xml->preserveWhiteSpace = false;

            //Se ingresa formato de salida
            $xml->formatOutput = true;

            //Se instancia el objeto
            $xml_string =$xml->saveXML();

            //Y se guarda en el nombre del archivo 'achivo.xml', y el obejto nstanciado
            \Storage::disk('local')->put('Contacto.xml',$xml_string);

            return response()->json(["intState"=>1,"strMensaje"=>"Información guardada correctamente.","contenido"=>$xml_string],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    /**
     * Ver el xml de configuración
     * @return [type] [description]
     */
    public function verXml() {
        $xml = \Storage::disk('local')->get('Contacto.xml');
        return response($xml)->withHeaders([ 'Content-Type' => 'text/xml']);
    }

    /**
     * Función para validar si es un email
     * @param  [type] $email Email
     * @return [type]        True o False
     */
    public function fnValidarEmail($email) {
    return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? true : false;
    }

    /**
     * Enviar mensaje y correo
     * @return [type] [description]
     */
    public function enviarEmail() {
        // Enviar email
        if(request()->ajax()){
            $dataRequest = request()->all();
            $contenido = "";

            $intState = 1;
            $strMensaje = "Información enviada.";

            $xml = simplexml_load_string(\Storage::disk('local')->get('Contacto.xml'));
            $emails = $xml->Email->Llegada;
            
            // Validar correo electrónico
            $validate = (new ContactoController)->fnValidarEmail($dataRequest['txtEmail']);

            if(isset($dataRequest['txtEmailCopia']) and !is_null($dataRequest['txtEmailCopia']) and $dataRequest['txtEmailCopia']!=""){
                $emails.= ";".$dataRequest['txtEmailCopia']; 
            }

            if ($validate) {
                $Contactos = Contactos::Email($dataRequest['txtEmail'])
                    ->get();
                
                if (count($Contactos) > 0) {
                    $Contactos2 = Contactos::where('email','=',$dataRequest['txtEmail'])->update(array('name' => $dataRequest['txtName'], 'tel' => $dataRequest['txtTelefono'], 'email' => $dataRequest['txtEmail'], 'created_at' => date('Y-m-d H-i-s')));

                    $ContactosMensajes = ContactosMensajes::create([
                        "contactos_id" => $Contactos[0]->id,
                        "asunto" => $dataRequest['txtAsunto'],
                        "mensaje" => $dataRequest['message'],
                        "sn_visto" => 1
                    ]);
                } else {
                    $Contactos = Contactos::create([
                        "name" => $dataRequest['txtName'],
                        "tel" => $dataRequest['txtTelefono'],
                        "email" => $dataRequest['txtEmail']
                    ]);

                    $ContactosMensajes = ContactosMensajes::create([
                        "contactos_id" => $Contactos->id,
                        "asunto" => $dataRequest['txtAsunto'],
                        "mensaje" => $dataRequest['message'],
                        "sn_visto" => 1
                    ]);
                }

                if (!empty($emails)) {
                    // Enviar Email
                    $emails = explode(";", $emails);
                    foreach ($emails as $key => $correo) {
                        Mail::to(''.$correo)->send(new MessageReceived($dataRequest));
                    }
                }
            } else {
                $intState = 0;
                $strMensaje = "Correo Electrónico Invalido.";
            }

            return response()->json(["intState"=>$intState,"strMensaje"=>$strMensaje,"contenido"=>$contenido],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    /**
     * Ver los mensajes en su proceso
     * @return [type] [description]
     */
    public function verContactos() {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $contenido = Contactos::Name($dataRequest['txtEmail'])
                ->Tel($dataRequest['txtEmail'])
                ->Email($dataRequest['txtEmail'])
                ->DtFechaInicio($dataRequest['dtpFechaInicio'])
                ->DtFechaFin($dataRequest['dtpFechaFin'])
                ->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Información obtenida.","contenido"=>$contenido],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    /**
     * Ver los mensajes en su proceso
     * @return [type] [description]
     */
    public function verMensajes() {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $contenido = ContactosMensajes::Contacto($dataRequest['contacto'])
                ->DtFechaInicio($dataRequest['dtpFechaInicio'])
                ->DtFechaFin($dataRequest['dtpFechaFin'])
                ->orderBy('id', 'DESC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Información obtenida.","contenido"=>$contenido],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    /**
     * Función para obtener la ip
     * @return [type] [description]
     */
    function get_client_ip_env() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    /**
     * Generar registro de visitas
     * @return [type] [description]
     */
    public function registrarVisita() {
        if(request()->ajax()){
            $contenido = "";
            $ipaddress = $this->get_client_ip_env();

            if (!empty($ipaddress) && $ipaddress != 'UNKNOWN') {
                $Visitas = Visitas::Ip($ipaddress)
                    ->Fecha(date('Y-m-d'))
                    ->orderBy('id', 'DESC')->get();
                
                if (count($Visitas) == 0) {
                    // Registrar visita
                    $Visitas = Visitas::create([
                        "ip" => $ipaddress
                    ]);
                }
            }

            return response()->json(["intState"=>1,"strMensaje"=>"Información agregada.","contenido"=>$ipaddress],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    /**
     * Total de visitas
     * @return [type] [description]
     */
    public function totalVisitas() {
        if(request()->ajax()){
            $contenido = array();

            $contenido['hoy'] = 0;
            $contenido['total'] = 0;

            // Hoy
            $visitasHoy = DB::table('visitas')
                ->select(DB::raw('COUNT(id) as total'))
                ->where('created_at', 'LIKE', date('Y-m-d')."%")
                ->get();
            if (count($visitasHoy) > 0) {
                $contenido['hoy'] = $visitasHoy[0]->total;
            }

            // Total
            $visitasTotal = DB::table('visitas')
                ->select(DB::raw('COUNT(id) as total'))
                ->get();
            if (count($visitasTotal) > 0) {
                $contenido['total'] = $visitasTotal[0]->total;
            }

            return response()->json(["intState"=>1,"strMensaje"=>"Información agregada.","contenido"=>$contenido],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {

    }

    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store()
    {

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update($id)
    {

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {

    }
  
}

?>