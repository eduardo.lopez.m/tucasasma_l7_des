<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class NosotrosController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            // Guardar Imagen
            $directorio = 'img-conoce-sma-nosotros/';
            $arrExtenciones["image/png"] = ".png";
            $arrExtenciones["image/jpg"] = ".jpg";
            $arrExtenciones["image/jpeg"] = ".jpeg";

            $ln_url_imagen = $directorio ."sinImagen.png";
            if(isset($dataRequest['ln_url_imagen'])) {
                $ln_url_imagen=$directorio."NosotrosPrincipal".$arrExtenciones[$_FILES['ln_url_imagen']['type']];
                if(!move_uploaded_file($_FILES['ln_url_imagen']['tmp_name'], $ln_url_imagen)){
                    $ln_url_imagen = $directorio ."sinImagen.png";
                }
            } else if(isset($dataRequest['txtLinkImagen'])) {
                $ln_url_imagen = $dataRequest['txtLinkImagen'];
            }

            $xml = new \DomDocument('1.0', 'UTF-8'); //Se crea el docuemnto

            $raiz = $xml->createElement('Nosotros');
            $raiz = $xml->appendChild($raiz);

            // Titulos
            $nodo_First = $xml->createElement('Titulos');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Principal', $dataRequest['txtPrinicipal']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Secundario', $dataRequest['txtSecundario']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Descripcion', $dataRequest['txtDescripcion']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Imagen', $ln_url_imagen);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            // Empresa
            $nodo_First = $xml->createElement('Empresa');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Vision', $dataRequest['txtMision']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Mision', $dataRequest['txtVision']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            //Se eliminan espacios en blanco
            $xml->preserveWhiteSpace = false;

            //Se ingresa formato de salida
            $xml->formatOutput = true;

            //Se instancia el objeto
            $xml_string =$xml->saveXML();

            //Y se guarda en el nombre del archivo 'achivo.xml', y el obejto nstanciado
            \Storage::disk('local')->put('Nosotros.xml',$xml_string);

            return response()->json(["intState"=>1,"strMensaje"=>"Información guardada correctamente.","contenido"=>$xml_string],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    public function verXml() {
        $xml = \Storage::disk('local')->get('Nosotros.xml');
        return response($xml)->withHeaders([ 'Content-Type' => 'text/xml']);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {

    }

    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store()
    {

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update($id)
    {

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {

    }
  
}

?>