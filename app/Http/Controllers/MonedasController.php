<?php

namespace App\Http\Controllers;

use App\Monedas;
use Illuminate\Http\Request;

class MonedasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monedas = Monedas::select(['ln_codigo_moneda','ln_desc_moneda'])
                            ->where('nu_activo', '1')
                            ->orderBy('ln_desc_moneda', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$monedas],200);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Monedas  $monedas
     * @return \Illuminate\Http\Response
     */
    public function show(Monedas $monedas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Monedas  $monedas
     * @return \Illuminate\Http\Response
     */
    public function edit(Monedas $monedas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Monedas  $monedas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Monedas $monedas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Monedas  $monedas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Monedas $monedas)
    {
        //
    }
}
