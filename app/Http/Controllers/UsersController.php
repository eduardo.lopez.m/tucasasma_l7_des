<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $usuarios = User::where('name', 'like', '%'.$dataRequest['name'].'%')->orderBy('name', 'ASC')->get();

            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron los usuarios correctamente.","usuarios"=>$usuarios],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","usuarios"=>""],400) ;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(request()->ajax()){
            $dataRequest = request()->all();
            //dd($dataRequest);
            $estatus=0;
            if(isset($dataRequest['nu_activo'])){
                $estatus=1;
            }

            $usuario=User::create([
                "name" => $dataRequest['name'], 
                "ln_direccion" => $dataRequest['ln_direccion'], 
                "ln_telefono" => $dataRequest['ln_telefono'], 
                "email" => $dataRequest['email'], 
                "password" => Hash::make($dataRequest['password']), 
                "nu_activo" => $estatus
            ]);
            return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente el usuario: ". $dataRequest['name'],"usuario"=>compact('usuario')],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","usuario"=>""],400) ;            
        }
    }

    public function show(User $user, $id)
    {
        if(request()->ajax()){
            $usuario = User::findOrFail($id);
            return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","usuario"=>compact("usuario")],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al buscar usuario.","usuario"=>""],400);
        }
    }

    public function update(Request $request, User $user, $id)
    {
        if(request()->ajax()){
            
            $dataRequest = request()->all();

            if (isset($dataRequest['nu_activo'])) {
                if(!is_numeric($dataRequest['nu_activo'])){
                    $dataRequest['nu_activo'] = '1';
                }
            }

            $usuario = User::findOrFail($id);

            if(empty($dataRequest['password'])){
                $dataRequest['password'] = $usuario->password;
            }else{
                $dataRequest['password'] = Hash::make($dataRequest['password']);
            }

            //dd($dataRequest);

            $usuario->update($dataRequest);
            
            return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente a: ".$usuario->name,"usuario"=>compact('usuario')],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al modificar.","usuario"=>""],400);
        } 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id)
    {
        if(request()->ajax()){
            $usuario = User::findOrFail($id);
            $usuario->delete();
            return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente a: ".$usuario->name,"usuario"=>$usuario],200);
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Problemas al eliminar.","usuario"=>""],400);
        }
    }

    public function fnLoginConductor(){
        $dataRequest = request()->all();
        $usuario = User::where('email',$dataRequest['ln_email'])->where('nu_activo', '1')->first();

        if($usuario && Hash::check($dataRequest['ln_contrasena'],$usuario->password)){
            return response()->json(["intState"=>1,"strMensaje"=>"Credenciales Correctas","usuario"=>$usuario],200) ;
        } 
        return response()->json(["intState"=>0,"strMensaje"=>"Credenciales Incorrectas","usuario"=>""],400) ;
    }
}
