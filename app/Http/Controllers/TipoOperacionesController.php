<?php

namespace App\Http\Controllers;

use App\TipoOperaciones;
use Illuminate\Http\Request;

class TipoOperacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipooperacion = TipoOperaciones::select(['nu_tipo_operacion','ln_tipo_operacion'])
                ->where('nu_activo', '1')
                ->orderBy('ln_tipo_operacion', 'ASC')->get();

        return response()->json(["intState"=>1, "datos"=>$tipooperacion],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoOperaciones  $tipoOperaciones
     * @return \Illuminate\Http\Response
     */
    public function show(TipoOperaciones $tipoOperaciones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoOperaciones  $tipoOperaciones
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoOperaciones $tipoOperaciones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoOperaciones  $tipoOperaciones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoOperaciones $tipoOperaciones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoOperaciones  $tipoOperaciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoOperaciones $tipoOperaciones)
    {
        //
    }
}
