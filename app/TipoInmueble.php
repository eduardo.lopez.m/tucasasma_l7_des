<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoInmueble extends Model
{
    protected $fillable = ['ln_tipo_inmueble','nu_activo'];
    protected $primaryKey = 'nu_tipo_inmueble';
    protected $hidden = ['updated_at', 'created_at'];
}
