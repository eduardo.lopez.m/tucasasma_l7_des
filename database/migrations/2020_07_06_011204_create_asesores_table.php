<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asesores', function (Blueprint $table) {
            $table->bigIncrements('nu_asesor');
            $table->string('ln_nombre');
            $table->string('ln_puesto');
            $table->string('ln_descripcion');
            $table->string('ln_link_facebook');
            $table->string('ln_link_twitter');
            $table->string('ln_link_linked');
            $table->string('ln_link_whattsapp');
            $table->string('ln_telefono');
            $table->string('ln_correo');
            $table->string('ln_url_imagen');
            $table->smallInteger('nu_activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asesores');
    }
}
