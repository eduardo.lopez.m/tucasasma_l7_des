<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarruselSmasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrusel_smas', function (Blueprint $table) {
            $table->bigIncrements('nu_publicidad');
            $table->string('ln_titulo');
            $table->string('ln_descripcion');
            $table->string('ln_url_imagen');
            $table->smallInteger('nu_activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrusel_smas');
    }
}
