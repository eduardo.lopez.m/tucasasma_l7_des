@extends('layoutsFront.app')
@section('content')

    <!-- Carousel -->
    <div class="slide-one-item home-slider owl-carousel" id="CarouselPrincipal">
    </div>

    <!-- Filtros & Orden -->
    <div class="site-section site-section-sm pb-0">
      <div class="container">
        <div class="row">
          <form class="form-search col-md-12" style="margin-top: -100px;">
            <div class="row  align-items-end">
              <div class="col-md-3">
                <label for="list-types">Tipo de Inmueble</label>
                <div class="select-wrap">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select id ="cmbTipoInmueble" name="list-types" id="list-types" class="form-control d-block rounded-0 selectTipoInmueble">
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="offer-types">Tipo de Oferta</label>
                <div class="select-wrap">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select id="cmbTipoOferta" name="offer-types" id="offer-types" class="form-control d-block rounded-0 selectTipoOferta">
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="select-city">Ciudad</label>
                <div class="select-wrap">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select name="select-city" id="select-city" class="form-control d-block rounded-0">
                    <option value="">San Miguel de Allende</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label></label>
                <input type="button" class="linkFiltrarPropiedades btn btn-success text-white btn-block rounded-0 btn-lg" value="Buscar">
              </div>
            </div>
          </form>
        </div>  

        <div class="row">
          <div class="col-md-12">
            <div class="view-options bg-white py-3 px-3 d-md-flex align-items-center">
              <div class="mr-auto" style="font-size: 18px; cursor:pointer;">
                <span class="icon-view_module"></span>
                <span class="icon-view_list"></span>
                
              </div>
              <div class="ml-auto d-flex align-items-center">
                <div>
                  <a style="cursor: pointer;" class="linkFiltrarPropiedaresReiniciar view-list px-3 border-right active">Todo</a>
                  <a style="cursor: pointer;" class="linkFiltrarPropiedaresRenta view-list px-3 border-right">En Renta</a>
                  <a style="cursor: pointer;" class="linkFiltrarPropiedaresVenta view-list px-3">En Venta</a>
                </div>

                <div class="select-wrap" style="width: 150px;">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select id="cmbOdernar"  class="form-control form-control-sm d-block rounded-0">
                    <option value="">Ordenar por</option>
                    <option value="1">Menor Precio</option>
                    <option value="2">Mayor Precio</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
       
      </div>
    </div>

    <!-- Casas Vista Grid -->
    <div class="site-section site-section-sm bg-light">
      <div class="container">

        <div id="divCasaVistaGrid" class="row">
          <div class="row mb-5">
          </div>
        </div>

        <div id="divCasaVistaList">
        </div>

        <br>
        <br>
        <div class="row">
          <div class="col-md-12 text-center">
            <a href="/Propiedades" class="btn btn-outline-primary rounded-0 py-2 px-5">Ver Más Propiedades</a>
          </div>  
        </div>
        
      </div>
    </div>

    <!-- Quines Somos --> 
    @include('frontend.nosotrosEncabezado')

    <!-- Banner -->
    <div class="section-banner bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 border-color">
            <a href="{{asset('Propiedades/1/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-house"></span>
              <h2 class="service-heading">Residencial</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4 border-color">
            <a href="{{asset('Propiedades/2/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-flat"></span>
              <h2 class="service-heading">Departamentos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a href="{{asset('Propiedades/3/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-location"></span>
              <h2 class="service-heading">Terrenos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- Blog --> 

    <div class="site-section ">
      <div class="container ">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-12">
            <div class="site-section-title text-center">
              <h2>Conocé San Miguel de Allende</h2>
              <p>En 2017 fue nombrada por la revista Travel + Leisure como la mejor ciudad del mundo por su calidad en el servicio, amabilidad, gastronomía, limpieza, experiencia de compras y movilidad además de su gran aportación cultural, belleza arquitectónica, y lugares de diversión</p>
            </div>
          </div>
        </div>
        
        <div class="slide-normal">
          <div class="slide-one-item home-slider owl-carousel " id="ConoceSMANormal">
          </div>
        </div>

        <div class="slide-responsive">
          <div class="slide-one-item home-slider owl-carousel " id="ConoceSMAResponsive">
          </div>
        </div>
      </div>
    </div>

    <!-- Seleccionar opcion del menu -->
    <script src="{{ asset('ajax/menuFront.js') }}"></script>

@endsection