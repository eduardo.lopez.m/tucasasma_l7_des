<!-- Quines Somos -->    
<div class="site-section ">
  <div class="container" id="idDivNosotrosClase">
    <div class="row">
      <div class="col-md-6" data-aos="fade-up" data-aos-delay="100" id="idImgNosotrosPrincipal">
        <!-- <img src="images/seguridadcompra.jpg" alt="Image" class="img-fluid"> -->
      </div>
      <div class="col-md-5 ml-auto"  data-aos="fade-up" data-aos-delay="200">
        <div class="site-section-title">
          <h2 id="idNosotrosPrincipal"></h2>
        </div>
        <p class="lead" id="idNosotrosSecundario"></p>
        <p id="idNosotrosDescripcion"></p>
        <p><a href="#" class="btn btn-outline-primary rounded-0 py-2 px-5">Contactanos</a></p>
      </div>
    </div>
  </div>
</div>