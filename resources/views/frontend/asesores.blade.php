@extends('layoutsFront.app')
@section('content')

    <!-- Nuestros Asesores -->
    <div class="site-section bg-light">
      <div class="container mt-6">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7">
            <div class="site-section-title text-center">
              <h2>Nuestros Asesores</h2>
              <p>Para hablar con un agente, puedes llamarnos o escribirnos.</p>
            </div>
          </div>
        </div>
        <div class="row" id="divAsesores">
        </div>
      </div>
    </div>

    <!-- Banner -->
    <div class="section-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 border-color" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/1/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-house"></span>
              <h2 class="service-heading">Residencial</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4 border-color" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/2/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-flat"></span>
              <h2 class="service-heading">Departamentos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4 " data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/3/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-location"></span>
              <h2 class="service-heading">Terrenos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="mdlEnviarCorreo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color: #1f3c88;">
              <h4 class="modal-title" id="myModalLabel" style="color: #FFFFFF;">Contactar</h4>
          </div>
          <div class="modal-body">
              <form action="" class="form-contact-agent" id="frmContacto">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="txtName">Nombre Completo <span id="txtNameError" style="color: red;" class="input-obligatorio hide">Requerido</span></label>
                  <input type="text" id="txtName" name="txtName"  placeholder="Nombre Completo" class="form-control">
                  <input type="hidden" id="txtEmailCopia" name="txtEmailCopia"  placeholder="Nombre Completo" class="form-control">
                </div>
                <div class="form-group">
                  <label for="txtEmail">Correo <span id="txtEmailError" style="color: red;" class=" input-obligatorio hide">Requerido</span></label>
                  <input type="email" id="txtEmail" name="txtEmail" placeholder="Correo"  class="form-control">
                </div>
                <div class="form-group">
                  <label for="txtTelefono">Teléfono <span id="txtTelefonoError" style="color: red;" class=" input-obligatorio hide">Requerido</span></label>
                  <input type="text" id="txtTelefono" name="txtTelefono" placeholder="Teléfono" class="form-control">
                </div>
                <div class="row form-group">
                  <div class="col-md-12">
                    <label class="font-weight-bold" for="txtAsunto">Asunto <span id="txtAsuntoError" style="color: red;" class=" input-obligatorio hide">Requerido</span></label>
                    <input type="text" id="txtAsunto" name="txtAsunto" class="form-control" placeholder="Agregar Asunto" value="">
                  </div>
                </div>
                <div class="row form-group" >
                  <div class="col-md-12">
                    <label class="font-weight-bold" for="txtMessage">Mensaje <span id="txtMessageError" style="color: red;" class=" input-obligatorio hide">Requerido</span></label> 
                    <textarea name="message" id="txtMessage" name="txtMessage" cols="30" rows="3" class="form-control" placeholder="Mensaje"></textarea>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <input type="button" value="Enviar Mensaje" id="btnEnviarMensaje" class="btn btn-primary  py-2 px-4 rounded-0">
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 

<script>

    

  $(document).ready(function(){
    $('#mdlAsesores').addClass('active');
    $('#mdlEnviarCorreo').on('shown.bs.modal', function (event) {

      $(".input-obligatorio").addClass("hide");
      
      var button = $(event.relatedTarget);

      $("#txtEmailCopia").val(button.data('correo'));
      $("#txtAsunto").val("Contactar a "+button.data('nombre'));
      $("#txtMessage").val("Solicito información");

    });

  });
</script>
@endsection