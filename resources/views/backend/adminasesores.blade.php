@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Inicio</a>
                </li>
                <li>
                    <a href="">Asesores</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Criterios de Búsqueda</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="frmFiltros">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" id="data_1">
                                        <label>Nombre</label> 
                                        <input name="txtNombre"  type="text" id="txtNombre" placeholder="Nombre Nombre" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id = "btnBuscar" class="btn btn-success btn-sm" type="button"><i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">Buscar</span></button>
                        <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#mdlFormulario"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Nuevo</span></button>
                    </div>
                </div>
            </div>

            <!-- TABLA DE BUSQUEDA -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Asesores</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="tblRegistros" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Nombe</th>
                                        <th>Puesto</th>
                                        <th>Descripción</th>
                                        <th>Teléfono</th>
                                        <th>Correo</th>
                                        <th>Imagen</th>
                                        <th>Activo</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="mdlFormulario" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-user modal-icon"></i>
                    <h4 class="modal-title">Asesor</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="frmFormulario" action="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4" >
                                <div class="form-group" id="ln_nombreError">
                                    <label for="ln_nombre">* Nombre</label>
                                    <input type="text" id="ln_nombre" name="ln_nombre" placeholder="Nombre Asesor" class="form-control" maxlength="100">
                                    <input type="hidden" id="nu_asesor" name="nu_asesor" placeholder="Asesor" class="form-control" readonly="true" >
                                </div>
                            </div>
                            <div class="col-md-4" >
                                <div class="form-group" id="ln_puestoError">
                                    <label for="ln_puesto">* Puesto</label>
                                    <input type="text" id="ln_puesto" name="ln_puesto" placeholder="Puesto" class="form-control" maxlength="100">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="ln_codigo">Imagen</label>
                                    <input type="file" id="ln_url_imagen" name="ln_url_imagen" accept="image/png, image/jpeg">
                                    <p class="help-block">(png y jpg)</p>
                                </div>
                            </div>
                            <div class="col-md-4" >
                                <div class="form-group" id="ln_correoError">
                                    <label for="ln_correo">* Correo</label>
                                    <input type="text" id="ln_correo" name="ln_correo" placeholder="Correo" class="form-control" maxlength="100">
                                </div>
                            </div>
                            <div class="col-md-4" >
                                <div class="form-group" id="ln_telefonoError">
                                    <label for="ln_telefono">* Teléfono</label>
                                    <input type="text" id="ln_telefono" name="ln_telefono" placeholder="Ejemplo: 1234567890" class="form-control" maxlength="500">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="nu_activo">Activo</label><br>
                                    <input type="checkbox" id="nu_activo" name="nu_activo" class="i-checks" style="position: absolute; opacity: 0;" checked>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="ln_descripcion">Descripción</label>
                                    <textarea id="ln_descripcion" name="ln_descripcion" placeholder="Descripción" class="form-control" maxlength="400" rows="2" style="resize: vertical;"></textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-12 text-center">
                                <img id="imgTemporal" src="" alt="" style="width: 20%;height: auto;">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="row">
                                <div class="col-md-12">
                                    <h2>Redes Sociales</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group m-b">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa fa-facebook-square"></span></button> 
                                        </span> 
                                        <input name="ln_link_facebook"  type="text" id="ln_link_facebook" placeholder="Ejemplo: https://www.facebook.com/" class="form-control" title="Link de Facebook">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group m-b">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa fa-twitter-square"></span></button> 
                                        </span> 
                                        <input name="ln_link_twitter"  type="text" id="ln_link_twitter" placeholder="Ejemplo: https://twitter.com/" class="form-control" title="Link Twitter">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group m-b">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa  fa-instagram"></span></button> 
                                        </span> 
                                        <input name="ln_link_linked"  type="text" id="ln_link_linked" placeholder="Ejemplo: https://www.instagram.com/" class="form-control" title="Link Instagram">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group m-b" id="nu_cod_paisError">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary">Código País</button> 
                                        </span> 
                                        <input name="nu_cod_pais" type="text" id="nu_cod_pais" placeholder="Ejemplo: 52" class="form-control soloNumeros" value="52" maxlength="2" title="WhatsApp Código">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group m-b" id="txtLinkWhatsError">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary"><span class="fa fa-whatsapp"></span></button> 
                                        </span> 
                                        <input name="ln_link_whattsapp"  type="text" id="ln_link_whattsapp" placeholder="Ejemplo: 1234567890" class="form-control soloNumeros" maxlength="10" title="WhatsApp Número">
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <strong>Nota</strong>
                                <p>Medidas de la imagen 400 x 400 preferentemente.</p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <button id="btnAgregarNuevo" type="button" class="btn btn-primary" style="display:none;">Agregar</button>
                    <button id="btnModificarRegistro" type="button" class="btn btn-primary" style="display:none;">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('ajax/adminasesores.js') }}"></script>

@endsection