@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Configuración Contacto</a>
                </li>
                <!-- <li>
                    <a href="">Carrusel Conoce SMA</a>
                </li> -->
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Información</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a> -->
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="frmFormulario" action="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="txtPrinicipalError">
                                        <label>Título Principal</label> 
                                        <input name="txtPrinicipal"  type="text" id="txtPrinicipal" placeholder="Título Principal" class="form-control" title="Título Principal">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="txtDescripcionError">
                                        <label>Descripción</label> 
                                        <textarea name="txtDescripcion"  type="text" id="txtDescripcion" placeholder="Descripción" class="form-control" title="Descripción"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="txtLinkMapsError">
                                        <label>Link Maps</label> 
                                        <input name="txtLinkMaps"  type="text" id="txtLinkMaps" placeholder="Ejemplo: https://www.google.com.mx/maps/" class="form-control" title="Link Maps">
                                    </div>
                                    <p><strong>Nota:</strong> Ir a <a target="_blank" href="https://goo.gl/maps/hZK4yk6RAzAKsMN37">Google Maps</a> buscar el sitio, posteriormente click en la opción compartir, seleccionar incorporar un mapa y copiar html.</p>
                                </div>
                                <div class="col-md-12">
                                    <br>
                                    <div class="form-group" id="txtEmailContactoError">
                                        <label>Email para correos de contacto</label> 
                                        <input name="txtEmailContacto"  type="text" id="txtEmailContacto" placeholder="Ejemplo: correo@dominio.com" class="form-control" title="Link Maps">
                                    </div>
                                    <p><strong>Nota:</strong> Para configurar varios correos la separación debe ser el caracter punto y coma ;</p>
                                    <p>Ejemplo: <strong>correo@dominio.com;correo2@dominio.com</strong></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id="btnGuardar" class="btn btn-success btn-sm" type="button"><i class="fa fa-save"></i>&nbsp;&nbsp;<span class="bold">Guardar</span></button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="{{ asset('ajax/contacto.js') }}"></script>

@endsection