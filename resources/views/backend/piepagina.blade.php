@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Configuración Pie de Página</a>
                </li>
                <!-- <li>
                    <a href="">Carrusel Conoce SMA</a>
                </li> -->
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Información</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a> -->
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="frmFormulario" action="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="txtPrinicipalError">
                                        <label>Título Principal</label> 
                                        <input name="txtPrinicipal"  type="text" id="txtPrinicipal" placeholder="Título Principal" class="form-control" title="Título Principal">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="txtDescripcionError">
                                        <label>Descripción</label> 
                                        <textarea name="txtDescripcion" rows="1" type="text" id="txtDescripcion" placeholder="Descripción" class="form-control" title="Descripción"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="txtDerechosError">
                                        <label>Derechos</label> 
                                        <input name="txtDerechos" type="text" id="txtDerechos" placeholder="Copyright ©2020 All rights reserved | Empresa" class="form-control" title="Derechos de Autor">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id="btnGuardar" class="btn btn-success btn-sm" type="button"><i class="fa fa-save"></i>&nbsp;&nbsp;<span class="bold">Guardar</span></button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="{{ asset('ajax/piepagina.js') }}"></script>

@endsection