@extends('layouts.app')
@section('content')

    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Configuración Nosotros</a>
                </li>
                <!-- <li>
                    <a href="">Carrusel Conoce SMA</a>
                </li> -->
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Información</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a> -->
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="frmFormulario" action="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Títulos</h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="txtPrinicipalError">
                                        <label>Principal</label> 
                                        <input name="txtPrinicipal"  type="text" id="txtPrinicipal" placeholder="Título Principal" class="form-control" title="Título Principal">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="txtSecundarioError">
                                        <label>Secundario</label> 
                                        <input name="txtSecundario"  type="text" id="txtSecundario" placeholder="Título Secundario" class="form-control" title="Título Secundario">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group" id="txtDescripcionError">
                                        <label>Descripción</label> 
                                        <textarea name="txtDescripcion"  type="text" id="txtDescripcion" placeholder="Descripción" class="form-control" title="Descripción"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ln_codigo">Imagen</label>
                                        <input type="file" id="ln_url_imagen" name="ln_url_imagen" accept="image/png, image/jpeg">
                                        <p class="help-block"><strong>Nota: </strong> Medidas de la imagen 690 x 460 preferentemente. (png y jpg)</p>
                                        <input type="hidden" name="txtLinkImagen" id="txtLinkImagen" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>Empresa</h2>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="txtMisionError">
                                        <label>Misión</label> 
                                        <textarea name="txtMision"  type="text" id="txtMision" placeholder="Misión" class="form-control" title="Misión"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="txtVisionError">
                                        <label>Visión</label> 
                                        <textarea name="txtVision"  type="text" id="txtVision" placeholder="Visión" class="form-control" title="Visión"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id="btnGuardar" class="btn btn-success btn-sm" type="button"><i class="fa fa-save"></i>&nbsp;&nbsp;<span class="bold">Guardar</span></button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="{{ asset('ajax/nosotros.js') }}"></script>

@endsection