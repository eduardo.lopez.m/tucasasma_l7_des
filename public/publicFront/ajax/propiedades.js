$(document).ready(function () {
    $('#mdlPropiedades').addClass('active');

    $(".view-list").click(function(){
        fnViewList();
    });

    $(".view-grid").click(function(){
        fnViewGrid();
    });

});

function fnViewList(){
    $(".view-list").addClass('active');
    $(".view-grid").removeClass('active');
    $( "#viewlist" ).fadeIn( "slow", function() {});
    $( "#viewgrid" ).fadeOut( "slow", function() {});
}

function fnViewGrid(){
    $(".view-grid").addClass('active');
    $(".view-list").removeClass('active');
    $( "#viewlist" ).fadeOut( "slow", function() {});
    $( "#viewgrid" ).fadeIn( "slow", function() {}); 
}