$(document).ready(function () {

    $('#mdlAdminContacto').addClass('active');

    $("#btnGuardar").click(function(){
        fnGuardarContacto();
    });

    fnObtenerInfoContacto(1, 'txtPrinicipal', 'txtDescripcion', 'txtLinkMaps', 'txtEmailContacto');

});

/**
 * Función para guardar la información de la configuración
 * @return {[type]} [description]
 */
function fnGuardarContacto() {
    var formData = new FormData(document.getElementById("frmFormulario"));
    formData.append("_method", 'POST');

    var nombreCampo = {
        'txtPrinicipal':'título principal',
        'txtDescripcion':'descripción',
        'txtLinkMaps':'link de maps',
        'txtEmailContacto':'correo de contacto',
    };

    var msg = fnValidarFormulario('frmFormulario', nombreCampo);
    if(msg){ return; }

    var infoData = fnAjaxSelect('POST','contactoAjax',formData);
    if(infoData.intState){
        swal({title: "Información!", text: infoData.strMensaje, type: "success"});
    } else {
        swal({title: "Información!", text: 'Ocurrio un problema, verifica con el administrador.', type: "warning"});
    }
}